//  app/models/bear.js

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CerdosSchema = new Schema({
	ra : String,
	numero : String,
	alimen : String,
	cartilla : String
});

module.exports = mongoose.model('Cerdos', CerdosSchema);